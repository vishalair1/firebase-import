import React, {useEffect, useState} from 'react';
import '../config/firebase';
import { authUi, auth } from '../config/firebase';
import 'firebaseui/dist/firebaseui.css';
import Button from '@material-ui/core/Button';


const Index = () => {
    const [user, setUser] = useState(false);
    const logoutUser = () => auth.signOut()
        .then(function() {
        console.log('Logged out');
        setUser(false);
      }, function(error) {
          console.log('Error Happened', error.message);
      });
    

    useEffect(() => {
        auth.onAuthStateChanged(function(u) {
            if (u) {
                const {email, uid} = u;
                setUser({email, uid});
              console.log(u, '---');
            } else {
                authUi('#firebaseui-auth-container');
            }
        });
    }, []);

    return (<div>
        <h2>Home</h2>
        <div id="firebaseui-auth-container">
        </div>
        {user && 
            <div>
                {user.email || user.uid}
                <br/>
                <Button variant="contained" color="primary"  onClick={logoutUser}> Logout </Button>
            </div>
        }
    </div>);
}
export default Index;