import React, { Component } from 'react';
import { HotTable } from '@handsontable/react'; 
import 'handsontable/dist/handsontable.full.css';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import '../App.css';

import { auth, database } from '../config/firebase';
const { log } = console;
// const ref = database.ref('imports').child('fergs');
// auth.signInAnonymously().then(() => {
//   log(ref);
//   ref.once('value').then(data => log(data))
// });

const styles = theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
});

function getSteps() {
  return ['Raw data', 'Select Columns', 'Mapped data'];
}

class App extends Component {
  constructor(props) {
    super(props);
    const data = [''];
    this.state = {
      activeStep: 0,
      skipped: new Set(),
      fullWidth: true,
      maxWidth: 'md',
      open: false,
      data: [],
      structuredData: [],
      headers: [],
      settings: {
        data: [data],
        licenseKey: 'non-commercial-and-evaluation',
        // minRows: 1,
        // minCols: 5,
        stretchH: 'all',
        autoColumnSize: true,
        width: '100%',
        // height: '580',
        rowHeaders: true,
        contextMenu: true,
        colHeaders:true
      }
    };

    this.hotTableComponent = React.createRef();
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick = () => {
    const {hotInstance} = this.hotTableComponent.current;
    const data = hotInstance.getData();
    const sdata = hotInstance.getSourceData();
    log('clicke', data, sdata, this.transpose(data));
    // let { settings } = this.state;
    // settings = Object.assign({}, settings, {minCols: 3, colHeaders: ['ID', 'Country','Code']});
    this.setState({open: true,data});
  }

  removeColumn() {
    const {hotInstance} = this.hotTableComponent.current;
    log(hotInstance);
    const totalCols = hotInstance.countCols();
    hotInstance.alter('remove_col', totalCols -1);
  }

  addColumn() {
    const {hotInstance} = this.hotTableComponent.current;
    const totalCols = hotInstance.countCols();
    hotInstance.alter('insert_col', totalCols);
  }

  transpose(a) {
    return a[0].map(function(_, c) {
      return a.map(function(r) {
        return r[c];
      });
    });
  }

  handleClose = () => {
    this.setState({open: false});
  }

  isStepOptional(step) {
    return step === 10;
  }

  isStepFailed(step) {
    return step === 10;
  }

  handleNext = () => {
    const { activeStep } = this.state;
    let { skipped } = this.state;
    this.setState({
      activeStep: activeStep + 1,
      skipped,
    });
  }

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  }

  handleImport = () => {
    const that = this;
    const ref = database.ref('imports').child('fergs');
    const structuredData = this.getDataWithHeader();
    auth.signInAnonymously().then(() => {
      structuredData.map(obj => ref.push(obj));
      that.handleNext();
    });
  }

  handleSkip = () => {
    const { activeStep } = this.state;
    if (!this.isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      throw new Error('You can\'t skip a step that isn\'t optional.');
    }

    this.setState(state => {
      const skipped = new Set(state.skipped.values());
      skipped.add(activeStep);
      return {
        activeStep: state.activeStep + 1,
        skipped,
      };
    });
  }

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  }

  setHeader = (id) => e => {
      const {headers} = this.state;
      headers[id] = e.target.value;
      this.setState({headers})
  }

  isStepSkipped = (step) => {
    return this.state.skipped.has(step);
  }

  getDataWithHeader = () => {
      const { data, headers } = this.state;
      const structuredData = data.map(row => {
          let newRow = {};
          row.map((col,colIndex) => {
              const index = headers[colIndex] || colIndex;
              newRow[index] = col;
              return newRow;
          })
          return newRow;
      })
      return structuredData;
  }

  switchComponent = () => {
    const { data, activeStep, headers } = this.state;
    switch(activeStep) {
        case 0:
            return (<pre> {JSON.stringify(data,undefined,2)} </pre>);
        case 1:
            const [row] = data;
            return (
            <div> {row.map((c,i) => (
                <div>
                    <TextField
                        id="standard-name"
                        label={"Column "+(i+1)}
                        margin="normal"
                        value = {headers[i] || ''}
                        onChange={this.setHeader(i)}
                    />
                    <br/>
                </div>
                ))}
            </div>);
        case 2:
            return (<pre> {JSON.stringify(this.getDataWithHeader(),undefined,2)} </pre>);
        case 3:
            return "Imported Successfully"
      default: 
        return activeStep;
    }
  }
  render() {
    const steps = getSteps();
    const { activeStep } = this.state;
    const { classes } = this.props;
      return (
        <div className="App">
          <Dialog onClose={this.handleClose} open={this.state.open} fullWidth={this.state.fullWidth}
          maxWidth={this.state.maxWidth} >
            <DialogTitle id="simple-dialog-title">Set backup account</DialogTitle>
            <DialogContent>
              <Stepper activeStep={activeStep}>
                {steps.map((label, index) => {
                  const props = {};
                  const labelProps = {};
                  if (this.isStepOptional(index)) {
                    labelProps.optional = (
                      <Typography variant="caption" color="error">
                        Alert message
                      </Typography>
                    );
                  }
                  if (this.isStepFailed(index)) {
                    labelProps.error = true;
                  }
                  if (this.isStepSkipped(index)) {
                    props.completed = false;
                  }
                  return (
                    <Step key={label} {...props}>
                      <StepLabel {...labelProps}>{label}</StepLabel>
                    </Step>
                  );
                })}
                
              </Stepper>
              
              <DialogContentText id="alert-dialog-description">
                {
                  this.switchComponent()
                }
              </DialogContentText>
            </DialogContent>
            <DialogActions>
            {steps.length === activeStep ? (
                  <div>
                    <Button variant="contained" color="secondary" onClick={this.handleReset}>Reset</Button>
                    <Button color="primary" onClick={this.handleClose}>Close</Button>
                  </div>
                ) : (
                  <div>
                    <div>
                      <Button
                        disabled={activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.backButton}
                      >
                        Back
                      </Button>
                      <Button variant="contained" color="primary" onClick={activeStep === steps.length - 1 ? this.handleImport: this.handleNext}>
                        {activeStep === steps.length - 1 ? 'Import' : 'Next'}
                      </Button>
                    </div>
                  </div>
                )}
          </DialogActions>
          </Dialog>
          <HotTable ref={this.hotTableComponent} settings={this.state.settings}/>
          <Button variant="contained" color="primary" className={classes.button}  onClick={this.handleClick.bind(this)}> Click </Button>
          <Button variant="contained" color="secondary" className={classes.button}  onClick={this.addColumn.bind(this)}> Add  </Button>
          <Button variant="contained" color="secondary" className={classes.button}  onClick={this.removeColumn.bind(this)}> remove </Button>
        </div>
      );
    }
}

export default  withStyles(styles)(App);
