import React, { Component } from 'react';
import 'handsontable/dist/handsontable.full.css';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import '../App.css';
import Loader from 'react-loader-spinner'

import { auth, database } from '../config/firebase';
const { log } = console;

const styles = theme => ({
  root: {
    width: '100%',
    margin: 'auto',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  loader: {
    margin: 'auto'
  }
});

function getSteps() {
  return ['Raw data', 'Select Columns', 'Mapped data'];
}
class TableComponent extends Component {
  state = {
    data: []
  };
  componentWillMount(){
    const data = [];
    const that = this;
    const ref = database.ref('imports').child('fergs');
    auth.signInAnonymously().then(() => {
      let rawData = {};
      ref.once('value').then(snap => {
        rawData = snap.val();
        for (let i in rawData){
          data.push(rawData[i]);
        }
        // that.setState({data});
      });
    });
  }
  render() {
    const {classes} = this.props;
    const {data} = this.state;
    const [firstRow] = data;
    const headers = data.length ? Object.keys(firstRow).filter(k => k !== 'null') : [];

    return (
      <Paper className={classes.root}>
        {data.length ? (
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                {
                  headers.map(cell =>(<TableCell align="right" key={cell}>{cell}</TableCell>))
                } 
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row,k) => (
                <TableRow key={k}>
                {
                  headers.map(cell =>(<TableCell align="right" key={cell}>{row[cell]}</TableCell>))
                } 
              </TableRow>
              ))}
            </TableBody>
          </Table>
        ): (
          <Loader 
            type="Oval"
            color="#00BFFF"
            height="100"	
            width="100"
            className="s"
            class={classes.loader}
          />
        )}
      </Paper>
    );
  }
}
export default  withStyles(styles)(TableComponent);
