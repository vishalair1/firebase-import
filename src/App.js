import React from 'react';
import './App.css';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import App from './components/App';
import Index from './components/Index';
import AppBar from './components/AppBar';
import Table from './components/Table';

// const Index = () => <h2>Home</h2>;
const About = () => <h2>About <br/> <Link to="/users">Users 1</Link></h2>;
const Users = () => <h2>Users <br/> <Link to="/about">About</Link></h2>;
const AppRouter = () => (
  <Router>
    <Route
        render={({ location }) => (
    <div>
      <AppBar> </AppBar>
      
      <div>{JSON.stringify(location.key)}</div>
      <TransitionGroup>
        <CSSTransition
          key={location.key}
          classNames="fade"
          timeout={300}
        >
          <Switch key={location}>
            <Route exact path="/" component={Index} />
            <Route exact path="/about" component={About} />
            <Route exact path="/users" component={Users} />
            <Route exact path="/app" component={App} />
            <Route exact path="/table" component={Table} />
            <Route render={() => <div>Not Found</div>} />
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    </div>)}
    />
  </Router>
);

export default AppRouter;
