import firebase from 'firebase/app';
import * as firebaseui from 'firebaseui';
import 'firebase/auth';
import 'firebase/database';

const {
    log
} = console;

log('firebase init');

const config = {
    apiKey: 'AIzaSyCuvQbWPzYA41pKFnfp9A0FqHBODpFqKdQ',
    authDomain: 'fir-push-notification-885ee.firebaseapp.com',
    databaseURL: 'https://fir-push-notification-885ee.firebaseio.com',
    projectId: 'fir-push-notification-885ee',
    storageBucket: 'fir-push-notification-885ee.appspot.com',
    messagingSenderId: '952224563874'
};
firebase.initializeApp(config);
const database = firebase.database();
const auth = firebase.auth();
const ui = new firebaseui.auth.AuthUI(firebase.auth());
const authUi = id => ui.start(id, {
    // signInSuccessUrl: '/',
    signInFlow: 'popup',
    privacyPolicyUrl: function() {
        window.location.assign('https://en.wikipedia.org/wiki/Proxima_Centauri');
    },
    tosUrl: 'https://en.wikipedia.org/wiki/Proxima_Centauri',
    callbacks: {
        signInSuccessWithAuthResult: function(authResult, redirectUrl) {
            console.log(authResult, redirectUrl);
            // User successfully signed in.
            // Return type determines whether we continue the redirect automatically
            // or whether we leave that to developer to handle.
            return false;
        }
    },
    signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
        // firebase.auth.GithubAuthProvider.PROVIDER_ID,
        // firebase.auth.PhoneAuthProvider.PROVIDER_ID,
        firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
    ],
});

export { auth, database, authUi };